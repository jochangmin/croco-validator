# README #

Croco Validator validate Google or IOS's In-App Purchase in Python

### How do I get set up? ###


```
#!bash

sudo pip install croco
```


### Example ###

```
#!python

In [1]: from croco import CrocoValidator

In [2]: public_key ='MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjvgWCa0oIWRq9L3AL3NaFGGHsc/n/N/EEVJGp4DtEd0lMC8xLiGr+b6/TDOiS4+pZpqeI88Gp5sGALoxrMwQgFIOVTrsY7EslmFLFC9WyaZTj8rUTycz2obkksreTy0AI0QkpftWtYLE0YJ5zeDOxvqRlh19X1ai+R0YK0Ozj97NvowHdUA9CMynpaG5CdlCW45fp6SvTt5jGWhBNI1QmL3QtnuJncQGj5po7PG3oBCX0EVzx3XPD24nTR3cdtNTBJ2hnPFytzWBNQZn6a3QdrZOwooLugOuROU+CjpThjZnGwtAGSjRYbvVBXVmW4rjT4bvGnrwZCGBZx50uNUhbwIDAQAB'

In [3]: receipt = '{"orderId":"12999763169054705758.1308363057157498","packageName":"com.inplay.metalslugtest","productId":"metal_testitem1","purchaseTime":1409644120225,"purchaseState":0,"purchaseToken":"cjijegkpnkegegbocbhgldpa.AO-J1OwTO4OxuqS38MSieY0jqPGw_ktz2N-ok7FGY2mKM97Cz9ihUcn3OKnHreE_97ztEFRI5bU8f10ZVn4Uh9D_l9IrBW2dkOupzNXfPCGeOC_x711qW6o_1YzZB1a3failSNi-2JrL"}'

In [4]: signature = 'c0Q4MZ/Y2Csy+Scf6nfRw11+X46xRiT5eJaED6owMt6y0QGjnGt7sB36Gu92ydGO2xCcbhfu7+uujug5zb3PNvck0DgfQ7WuSvyrRbDDq5MC+VmRb6i8xJ+14L2tHweXA6vqrDqv5vNFpExfAzGBwiAGNH7RVwQiB5Sue5nK3H0PslSvLqCZZIDgllOEThYG5jaHKhuVWiDTw9VPQKis8IFZzxS5Xxzy6IuT1YgiYTyV1x2uJbhhe2YJLyj4QUwGNgbcjsENIMZqaTkFvOr80Dj1UISDMlbNGB6pfxjOy5H4CqkEE2PzESBjTE9IGAjMz8Hjylw3iqbbmtkXhmj1aQ=='

In [5]: croco = CrocoValidator(public_key)

In [6]: croco.validate(receipt, signature)
Out[6]: True

```


### Who do I talk to? ###

* Anderson (Chang Min)
* a141890@gmail.com